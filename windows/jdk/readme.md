# 其他笔记-Windows安装JDK
## 下载JDK
下载地址
```
http://www.oracle.com/technetwork/java/javase/downloads/index.html
```
## 设置环境变量
```
JAVA_HOME=/path/to/jdk
PATH=%JAVA_HOME%\bin;
CLASSPATH=.;%JAVA_HOME%\lib\dt.jar;%JAVA_HOME%\lib\tools.jar
```
## 验证环境变量
```
java -version
```
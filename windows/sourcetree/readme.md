# SourceTree安装指南
#### SourceTree下载地址
官方下载（最新版）
```
https://www.sourcetreeapp.com/
```
腾讯下载（老版）
```
https://pc.qq.com/detail/17/detail_23237.html
```
**目前最最新版本3.0.17用次方法行不通了，建议使用老版。**
#### SourceTree免注册安装
先执行安装，在账号登录界面退出，然后找到用户本地文件夹下的SourceTree目录，找到accounts.json 文件，没有则新建它。<br>
将文件拷贝后，重新启动一次sourcetree，安装完成。<br>
```
C:\Users\Administrator\AppData\Local\Atlassian\SourceTree
```
accounts.json文件<br>
```
[
  {
    "$id": "1",
    "$type": "SourceTree.Api.Host.Identity.Model.IdentityAccount, SourceTree.Api.Host.Identity",
    "Authenticate": true,
    "HostInstance": {
      "$id": "2",
      "$type": "SourceTree.Host.Atlassianaccount.AtlassianAccountInstance, SourceTree.Host.AtlassianAccount",
      "Host": {
        "$id": "3",
        "$type": "SourceTree.Host.Atlassianaccount.AtlassianAccountHost, SourceTree.Host.AtlassianAccount",
        "Id": "atlassian account"
      },
      "BaseUrl": "https://id.atlassian.com/"
    },
    "Credentials": {
      "$id": "4",
      "$type": "SourceTree.Model.BasicAuthCredentials, SourceTree.Api.Account",
      "Username": "",
      "Email": null
    },
    "IsDefault": false
  }
]
```
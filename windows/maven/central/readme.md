# 其他笔记-如何向maven中央仓库提交jar
# 1.0JIRA准备
## 1.1注册一个JIRA账号
```
https://issues.sonatype.org/secure/Signup!default.jspa
```
## 1.2创建一个新工程的工单
注意！Project请选择‘Community Support - Open Source Project Repository Hosting (OSSRH)’<br>
```
https://issues.sonatype.org/secure/CreateIssue!default.jspa
```
## 1.3填写工单内容
注意！只需要填写<br>
Group Id：项目Maven的GroupID，注意，GroupID是一个需要你购买了的域名，否则审核员会DISS你。<br>
Project URL：你的git项目主页，如：（https://gitee.com/w6513017/HDIS-Framework）<br>
SCM url：可拉取项目的git地址，如：（https://gitee.com/w6513017/HDIS-Framework.git）<br>
## 1.4等待审核
进入工单页面查看Activity ===> Comments部分，此处为你和审核员的聊天内容。<br>
## 1.5审核完毕
当页面上审核状态（Status）为resolved时，才可以向中央仓库提交jar包。<br>
审核通过后会出现以下内容：<br>
Permalink<br>
twatson Thad Watson added a comment - Yesterday<br>
Configuration has been prepared, now you can:<br>
Deploy snapshot artifacts into repository https://oss.sonatype.org/content/repositories/snapshots<br>
Deploy release artifacts into the staging repository https://oss.sonatype.org/service/local/staging/deploy/maven2<br>
Promote staged artifacts into repository 'Releases'<br>
Download snapshot and release artifacts from group https://oss.sonatype.org/content/groups/public<br>
Download snapshot, release and staged artifacts from staging group https://oss.sonatype.org/content/groups/staging<br>
please comment on this ticket when you promoted your first release, thanks<br>
注意最后一句，当你发布第一个release后，请通知审核员一声。否则他不会为你激活向中央仓库同步。到时候你就玩蛇了！<br>
# 2.0Maven部署准备
## 2.1完善POM信息
需要在POM中完善name、description、url、license、scm、developer的信息<br>
```
<name>HDIS-Framework</name>
<description>支撑Java项目的，基于SpringBoot、阿里云的一系列公共组件，规定的一系列架构约束。</description>
<url>https://gitee.com/w6513017/HDIS-Framework</url>
<licenses>
    <license>
        <name>Apache License Version 2.0</name>
        <url>http://www.apache.org/licenses/LICENSE-2.0</url>
    </license>
</licenses>
<scm>
    <url>https://gitee.com/w6513017/HDIS-Framework.git</url>
    <connection>scm:git:https://gitee.com/w6513017/HDIS-Framework.git</connection>
    <developerConnection>scm:git:https://gitee.com/w6513017/HDIS-Framework.git</developerConnection>
    <tag>master</tag>
</scm>
<developers>
    <developer>
        <name>Nicholas.ZW.H</name>
        <email>w6513017@163.com</email>
    </developer>
</developers>
```
## 2.2配置distributionManagement
需要在POM文件中配置distributionManagement<br>
```
<distributionManagement>
    <!--快照版本地址-->
    <snapshotRepository>
        <id>snapshots</id>
        <url>https://oss.sonatype.org/content/repositories/snapshots</url>
    </snapshotRepository>
    <!--正式版本地址-->
    <repository>
        <id>releases</id>
        <url>https://oss.sonatype.org/service/local/staging/deploy/maven2</url>
    </repository>
</distributionManagement>
```
## 2.3配置JIRA的账号和密码
在/home/.m2/settings.xml中设置JIRA的账号和密码<br>
```
<settings>
  <servers>
    <server>
      <id>snapshots</id>
      <username>你的用户名</username>
      <password>你的密码</password>
    </server>
    <server>
      <id>releases</id>
      <username>你的用户名</username>
      <password>你的密码</password>
    </server>
  </servers>
</settings>
```
## 2.4配置上传插件
配置源代码插件、文档插件<br>
```
<build>
    <plugins>
        <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-source-plugin</artifactId>
            <version>2.4</version>
            <executions>
                <execution>
                    <id>attach-sources</id>
                    <goals>
                        <goal>jar</goal>
                    </goals>
                </execution>
            </executions>
        </plugin>
        <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-javadoc-plugin</artifactId>
            <version>2.10.4</version>
            <executions>
                <execution>
                    <id>attach-javadocs</id>
                    <goals>
                        <goal>jar</goal>
                    </goals>
                </execution>
            </executions>
        </plugin>
    </plugins>
</build>
```
## 2.5gpg相关配置（坑已踩完，放心使用）
### 2.5.1安装gpg程序
#### 2.5.1.1windows
下载gpg。注意！请选择GnuPG 1.4版本，高版本插件支持有问题！<br>
下载地址：<br>
```
https://www.gnupg.org/download/index.html
```
配置环境变量<br>
将gpg的安装目录配置进path<br>
#### 2.5.1.2linux
安装gnupg<br>
```
yum install -y gnupg
```
安装rng-tools，补充熵池。否则它在生成密钥的时候会卡住！<br>
```
yum install -y rng-tools
```
配置rng-tools<br>
```
echo 'EXTRAOPTIONS="--rng-device /dev/urandom"' >/etc/sysconfig/rngd
service rngd restart
chkconfig rngd on
```
### 2.5.2生成gpg密钥与上传gpg密钥
#### 2.5.2.1生成密钥
按照提示一步步执行即可<br>
```
gpg --gen-key
```
#### 2.5.2.2查看密钥
使用命令查看key<br>
```
gpg --list-keys
```
#### 2.5.2.3上传公钥
得到密钥以后需要上传至公钥服务器。<br>
maven在发布release版本时需要在公钥服务器上拉取密钥，进行验证。<br>
以下为公钥服务器列表，国内只有它能访问！公钥服务器之间会自动同步。<br>
```
keyserver.ubuntu.com
```
上传公钥<br>
```
gpg --keyserver hkp://keyserver.ubuntu.com --send-keys [公钥ID]
```
验证公钥<br>
```
gpg --keyserver hkp://keyserver.ubuntu.com --recv-keys [公钥ID]
```
### 2.5.3密钥相关操作（可跳过浏览）
#### 2.5.3.1导出密钥
导出公钥<br>
```
gpg --armor --output public-key --export [用户ID]
```
导出私钥<br>
```
gpg --armor --output private-key --export-secret-keys [用户ID]
```
#### 2.5.3.2导入密钥
导入密钥<br>
```
gpg --import [密钥文件]
```
从公钥服务器导入公钥<br>
```
gpg --keyserver hkp://subkeys.pgp.net --search-keys [用户ID]
```
#### 2.5.3.3删除密钥
删除私钥<br>
```
gpg --delete-secret-keys [用户uid]
```
删除公钥<br>
```
gpg --delete-keys [用户uid]
```
### 2.5.4配置gpg加密插件
由于发布releases环境才会用到gpg，因此可以设置profile进行环境的隔离。<br>
注意!插件版本必须使用1.4版本，1.4以上版本会卡住。<br>
注意!properties需要设置gpg.exe地址。<br>
```
<properties>
    <gpg.executable>D:\Program Files (x86)\GNU\GnuPG\gpg.exe</gpg.executable>
</properties>
<build>
    <plugins>
        <plugin>
           <groupId>org.apache.maven.plugins</groupId>
           <artifactId>maven-gpg-plugin</artifactId>
           <version>1.4</version>
           <executions>
             <execution>
               <id>sign-artifacts</id>
               <phase>verify</phase>
               <goals>
                 <goal>sign</goal>
               </goals>
             </execution>
           </executions>
        </plugin>
     </plugins>
</build>
```
# 3.0部署
## 3.1发布snapshot版本
### 3.1.1Maven设置snapshot版本
修改version，并加上‘-SNAPSHOT’后缀<br>
```
-SNAPSHOT
```
### 3.1.2发布
注意：snapshot版本提交时，并不需要gpg签名，直接提交即可。<br>
执行：<br>
```
mvn clean deploy
```
### 3.1.3验证
在snapshot仓库中可以看到已经发布的snapshot版本<br>
snapshot仓库地址：<br>
```
https://oss.sonatype.org/content/repositories/snapshots/
```
## 3.2提交release版本
### 3.2.1Maven设置release版本
修改version，并取消‘-SNAPSHOT’后缀<br>
### 3.2.2发布
注意：release版本提交时，需要gpg签名。<br>
执行：<br>
```
mvn clean deploy -P release
```
### 3.2.3登录sonatype执行发布操作
#### 3.2.3.1登录
```
https://oss.sonatype.org/#stagingRepositories
```
#### 3.2.3.2验证
查看你所提交的Repository。<br>
勾选需要sonatype验证的Repository，点击菜单栏的Close。<br>
在下方状态栏中（Activity）可以看见sonatype正在执行验证，并且会实时的显示验证结果<br>
#### 3.2.3.3发布
当验证完成后！<br>
查看你所提交的Repository。<br>
勾选需要sonatype发布的Repository，点击菜单栏的Release。<br>
在下方状态栏中（Activity）可以看见sonatype正在执行发布，并且会实时的显示验证结果<br>
#### 3.2.3.4验证
整个Repository消失后，表示发布成功，可以在仓库中找到了。<br>
sonatype仓库地址：<br>
```
https://oss.sonatype.org/content/repositories/releases/
```
过一段时间后，中央仓库会从sonatype仓库同步。中央仓库查看地址：<br>
```
http://mvnrepository.com/
```
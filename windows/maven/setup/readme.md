# 其他笔记-Windows安装Maven
## 前置条件
需要安装JDK<br>
## 下载maven
```
https://maven.apache.org/download.cgi
```
## 设置环境变量
```
PATH=[/path/to/maven]/bin
```
## 验证
```
mvn -v
```
## 修改maven配置文件
首先拷贝一份原生的settings.xml到C:\Users\[user]\.m2\settings.xml<br>
进入C:\Users\[user]\.m2\settings.xml<br>
将阿里云中央仓库配置进镜像。<br>
阿里云有很多仓库，此仓库为最大最全的中央仓库。<br>
```
<settings>
    <mirrors>
        <mirror>
            <id>AliYunCentral</id>
            <mirrorOf>central</mirrorOf>
            <name>AliYunCentral</name>
            <url>https://maven.aliyun.com/repository/central</url>
        </mirror>
    </mirrors>
</settings>
```
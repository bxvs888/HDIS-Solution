# 其他笔记-JAVA程序员IDEA必备插件推荐
## IDE Features Trainer
IDEA快捷键训练课程，快速帮你了解IDEA常用快捷键。<br>
交互式界面，一遍操作，一边学习。<br>
#### 使用方法：
在1-project窗口旁边可以找到与使用。<br>
## .ignore
一键创建各种ignore文件的模板，并且在编写.ignore文件时还可智能提示。<br>
#### 使用方法：
新建文件时，可以创建ignore模板。<br>
编辑.ignore文件时，会有智能提示。<br>
## Alibaba Java Coding Guidelines
阿里巴巴JAVA规范扫描插件，你懂的。<br>
可以扫描整个项目，且大部分可以自动修复。<br>
更多可看: [阿里Java编程规约](https://github.com/alibaba/p3c)<br>
#### 使用方法：
在需要你扫描的文件或文件夹上点击右键，选中编码规约扫描即可。<br>
记得把扫描结果改为显示为中文的。<br>
## GsonFormat
一键根据json文本生成java类，非常方便。<br>
当接口对接，别人给你提供json返回值时，使用此插件就能很快生成接收对象了。<br>
#### 使用方法：
在java类中使用alt+insert弹出快速生成界面选中GsonFormat，填入需要解析的Json即可。<br>
## GenerateAllSetter
一键调用一个对象的所有set方法并且赋予默认值，在对象字段多的时候非常方便。<br>
解决了在大对象set的时候，set多了不知道该set哪个了的问题。<br>
#### 使用方法：
在调用set方法时自动弹出。<br>
## Markdown Navigator
Markdown编辑工具，比Markdown Support好用！
## Lombok plugin
使用Lombok时，需要配合Lombok plugin
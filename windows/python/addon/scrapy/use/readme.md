# 常用命令一览
## 初始化项目
```
scrapy startproject [name]
```
## 创建Spider
```
scrapy genspider [name] [url]
```
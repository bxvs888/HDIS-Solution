# 常用命令一览
## 创建环境
## 切换环境
```
activate [name]
```
## 删除环境
## 确认当前环境
```
conda info -envis
```
# 运维笔记-Linux下安装与使用rinetd
## 安装与运行
```
wget http://www.boutell.com/rinetd/http/rinetd.tar.gz
tar -xvf rinetd.tar.gz
mv rinetd /root
cd /root/rinetd
sed -i 's/65536/65535/g' rinetd.c
mkdir /usr/man
make&&make install
echo "0.0.0.0 6379 [redis host 地址] 6379" >> /etc/rinetd.conf
echo "logfile /var/log/rinetd.log" >> /etc/rinetd.conf
rinetd
```
## 配置自启动
```
echo rinetd >>/etc/rc.local
```
# 运维笔记-Linux下git安装
## 安装git
下载地址<br>
```
yum install -y git
```
## 配置GIT用户名称与邮箱
```
git config --global user.name "你的名字"
git config --global user.email "你的邮箱"
```
## 记住密码
 ```
 git config --global credential.helper store
 ```
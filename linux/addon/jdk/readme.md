# 运维笔记-Linux下JDK安装
## 下载JDK
下载地址<br>
```
http://www.oracle.com/technetwork/java/javase/downloads/index.html
```
# yum方式安装
```
yum install -y /path/to/package.rpm
```
# 压缩方式安装JDK
## 解压缩
```
cd /usr/bin
tar zxvf jdk1.8.0_151.tar.gz
```
## 设置环境变量
```
vi /etc/profile
JAVA_HOME=/usr/bin/jdk1.8.0_151
PATH=$PATH:$JAVA_HOME/bin
CLASSPATH=.:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar
```
## 执行更改
```
source /etc/profile
```
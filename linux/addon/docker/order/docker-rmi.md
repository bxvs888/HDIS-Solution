# 删除镜像命令
## 删除单个镜像
```
docker rmi [镜像ID]
docker rmi [镜像名称:版本]
```
## 删除没有打标签的镜像
```
docker rmi `docker images -q | awk '/^<none>/ { print $3 }'`
```
## 根据镜像包含的关键字删除镜像
```
docker rmi --force `docker images | grep [关键字] | awk '{print $3}'`
```
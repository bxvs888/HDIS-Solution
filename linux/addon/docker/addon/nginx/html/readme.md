# Docker运维笔记-Nginx静态网页部署
## 打包
将静态网页、default.conf、Dockerfile拷贝到同一个目录下<br>
default.conf：<br>
```
server {
    listen       80;
    server_name  localhost;

    #charset koi8-r;
    #access_log  /var/log/nginx/host.access.log  main;

    location / {
        root   /usr/share/nginx/html;
        index  dist/index.html;
    }

    #error_page  404              /404.html;

    # redirect server error pages to the static page /50x.html
    #
    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root   /usr/share/nginx/html;
    }

    # proxy the PHP scripts to Apache listening on 127.0.0.1:80
    #
    #location ~ \.php$ {
    #    proxy_pass   http://127.0.0.1;
    #}

    # pass the PHP scripts to FastCGI server listening on 127.0.0.1:9000
    #
    #location ~ \.php$ {
    #    root           html;
    #    fastcgi_pass   127.0.0.1:9000;
    #    fastcgi_index  index.php;
    #    fastcgi_param  SCRIPT_FILENAME  /scripts$fastcgi_script_name;
    #    include        fastcgi_params;
    #}

    # deny access to .htaccess files, if Apache's document root
    # concurs with nginx's one
    #
    #location ~ /\.ht {
    #    deny  all;
    #}
}
```
Dockerfile：<br>
```
FROM nginx
ARG FILE_PATH
ADD default.conf /etc/nginx/conf.d
ADD ${FILE_PATH} /usr/share/nginx/html
```
镜像打包命令<br>
```
docker build -t [容器名称] --build-arg FILE_PATH=[文件地址] ./
```
## 运行
```
docker run --name wowgo-service-business -d -p 7070:80 wowgo-service-business
```
# Docker运维笔记-jar包部署
## 打包
将jar包、Dockerfile拷贝到同一个目录下<br>
Dockerfile：<br>
```
FROM java:8-jre
ARG FILE_PATH
ADD ${FILE_PATH} /root/Application.jar
ENTRYPOINT java -jar /root/Application.jar
```
镜像打包命令<br>
```
docker build -t [容器名称] --build-arg FILE_PATH=[文件地址] ./
```
## 运行
```
docker run --name wowgo-service-business -d -p 8080:8080 wowgo-service-business
```
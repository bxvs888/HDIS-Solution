# Docker运维笔记-DockerUI部署
## 拉取docker-ui镜像
```
docker pull uifd/ui-for-docker
```
## 运行docker-ui
```
docker run \
--rm \
-d \
--name docker-ui \
-p 9000:9000 \
--privileged \
-v /var/run/docker.sock:/var/run/docker.sock \
uifd/ui-for-docker
```
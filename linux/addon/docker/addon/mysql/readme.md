# Docker运维笔记-Mysql部署
## 官方教程
https://hub.docker.com/_/mysql/
## 拉取mysql镜像
```
docker pull mysql
```
## 运行mysql
```
docker run --name mysql -p 8993:3306 -v /var/lib/mysql:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=Pass@654321 -d mysql:latest
```
## 登录mysql，允许远程访问
```
mysql -u root -p
grant all privileges  on *.* to root@'%' identified by "Pass@654321";
flush privileges;
```
## 配置mysql编码
配置文件位置/etc/mysql/mysql.conf.d<br>
```
character_set_server=utf8
cat > mysqld.cnf <<EOF
EOF
```
## 查看mysql编码
```
show variables like 'character%';
```
# Docker运维笔记-MongoDB部署
## 官方教程
```
https://hub.docker.com/r/library/mongo/
```
## Docker部署步骤
第一步：拉取mongodb镜像<br>
```
docker pull mongo
```
第二步：启动mongodb，并且初始化权限<br>
```
docker run -p 27017:27017 --name mongodb-27017-zhgl -d mongo --auth
```
第三步：进入权限初始化模式的mongodb<br>
```
docker exec -it mongodb-27017-zhgl mongo admin
```
第四步：使用权限初始化模式建立超级管理员<br>
```
db.createUser({user:'mongodb_root',pwd:'mongodb_root',roles:[{role:'root',db:'admin'}]});
```
第五步：使用超级管理员(要重新登录)，创建数据库管理员<br>
```
db.createUser({user:'zhgl',pwd:'zhgl',roles:[{role:'readWrite',db:'zhgl'}]});
```
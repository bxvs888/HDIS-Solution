# Docker运维笔记-Redis部署
## 官方教程
```
https://hub.docker.com/_/redis/
```
## 拉取redis镜像
```
docker pull redis
```
## 启动redis
```
docker run -d --name redis -p 6379:6379 redis
```
# Docker运维笔记-Linux下Docker安装
## 下载docker的RPM文件
版本为docker-ce-17.03.2.ce-1.el7.centos.x86_64.rpm，此版本通过k8s测试<br>
## 使用yum命令安装
```
yum install -y https://download.docker.com/linux/centos/7/x86_64/stable/Packages/docker-ce-selinux-17.03.2.ce-1.el7.centos.noarch.rpm
yum install -y https://download.docker.com/linux/centos/7/x86_64/stable/Packages/docker-ce-17.03.2.ce-1.el7.centos.x86_64.rpm
```
# 配置Docker镜像加速服务
镜像加速地址可选用阿里云容器镜像加速器<br>
```
mkdir -p /etc/docker
tee /etc/docker/daemon.json <<-'EOF'
{
  "registry-mirrors": ["你的镜像加速器地址"]
}
EOF
```
# 运行docker
```
systemctl daemon-reload
systemctl enable docker
systemctl start docker
```
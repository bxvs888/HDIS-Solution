# 运维笔记-Linux下Maven安装
## 前置条件
需要安装JDK<br>
## 下载maven
```
https://maven.apache.org/download.cgi
```
## 解压缩maven
```
tar zxvf apache-maven-3.5.0-bin.tar.gz
```
## 设置环境变量
```
vi /etc/profile
MAVEN_HOME=/root/maven
PATH=${PATH}:${MAVEN_HOME}/bin
ln –s /root/maven/bin/mvn /usr/bin/mvn
```
## 执行更改
```
source /etc/profile
```
## 验证
```
mvn -v
```
## 配置setting.xml
首先新建.m2文件夹并拷贝一份原生的settings.xml进去<br>
```
mkdir /home/.m2
mv settings.xml /home/.m2
```
## 修改maven配置文件
进入/home/.m2/settings.xml<br>
将阿里云中央仓库配置进镜像。<br>
阿里云有很多仓库，此仓库为最大最全的中央仓库。<br>
```
<settings>
    <mirrors>
        <mirror>
            <id>AliYunCentral</id>
            <mirrorOf>central</mirrorOf>
            <name>AliYunCentral</name>
            <url>https://maven.aliyun.com/repository/central</url>
        </mirror>
    </mirrors>
</settings>
```
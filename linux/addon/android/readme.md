# 运维笔记-Linux下Android安装指南
## 前提条件
需要安装JDK<br>
## 安装Android
下载地址<br>
```
https://www.androiddevtools.cn/
```
解压缩<br>
```
tar -zxvf android.tgz
```
## 配置gradle
配置环境变量<br>
```
vi /etc/profile
ANDROID_HOME=/root/android
PATH=${PATH}:${ANDROID_HOME}/tools
PATH=${PATH}:${ANDROID_HOME}/platform-tools
```
执行更改<br>
```
source /etc/profile
```
## 验证
```
android -h
```
## 查看需要下载的组件
```
android list sdk --all
```
## 下载组件
需要下载<br>
Android SDK Tools<br>
Android SDK Platform-tools<br>
Android SDK Build-tools<br>
SDK Platform Android<br>
```
android update sdk -u --all --filter 1,2,4,47
```
# 运维笔记-Linux下nodeJs安装指南
## 安装
下载地址<br>
```
https://nodejs.org/en/download/
```
解压缩<br>
```
tar -xf node.tar.xz
```
## 配置
配置环境变量<br>
```
vi /etc/profile
NODE_HOME=/root/node
PATH=$PATH:$NODE_HOME/bin
```
执行更改<br>
```
source /etc/profile
```
## 验证
```
node -v
```
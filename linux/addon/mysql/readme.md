# 运维笔记-Linux下Mysql安装教程
## 下载
下载地址：<br>
```
https://dev.mysql.com/downloads/mysql/
```
下载mysql-5.7.21-1.el7.x86_64.rpm-bundle.tar<br>
## 预处理
解压缩<br>
```
tar -xvh mysql-5.7.21-1.el7.x86_64.rpm-bundle.tar
```
查询不需要的依赖<br>
```
rpm -qa|grep mariadb
```
删除不需要的依赖<br>
```
rpm -e --nodeps mariadb-libs-5.5.41-2.el7_0.x86_64
```
## 安装
依次按顺序安装<br>
```
rpm -ivh mysql-community-common-5.7.9-1.el7.x86_64.rpm
rpm -ivh mysql-community-libs-5.7.9-1.el7.x86_64.rpm
rpm -ivh mysql-community-client-5.7.9-1.el7.x86_64.rpm
rpm -ivh mysql-community-server-5.7.9-1.el7.x86_64.rpm
```
## 配置
root用户下初始化mysql<br>
```
/bin/mysqld --initialize --user=mysql
```
启动mysql<br>
```
systemctl start mysqld
```
查看root密码<br>
```
vi  /var/log/mysqld.log
```
登录<br>
```
mysql -u root -p
```
修改密码<br>
```
alter user 'root'@'localhost'  identified  by  'mYsqL$%123';
```
# 运维笔记-Linux下Jenkins部署指南
## 前提条件
需要安装Maven<br>
需要安装Docker<br>
需要安装GIT<br>
## docker构建
### 构建命令（例）
```
#!/bin/sh -l
cd ${WORKSPACE}
mvn clean package
cp Dockerfile ./target
cd target
docker stop wowgo-service-business|| echo "continue execute"
docker rm wowgo-service-business|| echo "continue execute"
docker rmi wowgo-service-business|| echo "continue execute"
docker build -t wowgo-service-business --build-arg FILE_PATH=Application.jar ./ || echo "continue execute"
docker run --name wowgo-service-business -d -p 8080:8080 wowgo-service-business|| echo "continue execute"
```
## k8s微服务构建
### jar构建命令（例）
```
#!/bin/sh -l
cd ${WORKSPACE}
mvn clean package
cp Dockerfile ./target
cd target
docker rmi registry.cn-hangzhou.aliyuncs.com/15928745302/wowgo-service-business || echo "continue execute"
docker build -t registry.cn-hangzhou.aliyuncs.com/15928745302/wowgo-service-business --build-arg FILE_PATH=Application.jar ./ || echo "continue execute"
docker login -u=w6513017@163.com registry.cn-hangzhou.aliyuncs.com -p fuck2you
docker push registry.cn-hangzhou.aliyuncs.com/15928745302/wowgo-service-business
kubectl --kubeconfig /root/.kube/config delete pod -l app=wowgo-service-business -n wowgo-develop
```
### nginx构建命令（例）
```
#!/bin/sh -l
cd ${WORKSPACE}
docker rmi registry.cn-hangzhou.aliyuncs.com/15928745302/wowgo-service-business || echo "continue execute"
docker build -t registry.cn-hangzhou.aliyuncs.com/15928745302/wowgo-service-business --build-arg FILE_PATH=./ ./ || echo "continue execute"
docker login -u=w6513017@163.com registry.cn-hangzhou.aliyuncs.com -p fuck2you
docker push registry.cn-hangzhou.aliyuncs.com/15928745302/wowgo-service-business
kubectl --kubeconfig /root/.kube/config delete pod -l app=wowgo-service-business -n wowgo-develop
```
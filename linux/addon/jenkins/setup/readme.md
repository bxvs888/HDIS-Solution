# 运维笔记-Linux下Jenkins安装指南
备注：由于Jenkins需要操作容器集群，因此不安装在容器内部<br>
## 前提条件
需要安装JDK<br>
## 使用war安装
下载地址<br>
```
https://jenkins.io/doc/book/installing/#war-file
wget http://mirrors.shu.edu.cn/jenkins/war-stable/2.121.2/jenkins.war
```
## 配置自启动
将jenkins.service拷贝至/etc/systemd/system<br>
jenkins.service：<br>
```
[Unit]
Description=jenkins

[Service]
ExecStart=/usr/bin/nohup java -jar /root/jenkins.war --httpPort=80
Restart=always
StartLimitInterval=0
RestartSec=10

[Install]
WantedBy=multi-user.target
```
运行命令执行配置<br>
```
systemctl daemon-reload
systemctl enable jenkins.service
systemctl restart jenkins.service
journalctl -xe
```
# 运行Jenkins
```
nohup java -jar jenkins.war --httpPort=80
```
## 安装插件
### 更换插件仓库地址
打开：jenkins->系统管理->管理插件->高级->升级站点<br>
仓库<br>
```
ftp://mirrors.shu.edu.cn/jenkins/
```
仓库一览<br>
```
http://mirrors.jenkins-ci.org/status.html
```
### 安装maven插件
jenkins->系统管理->管理插件->可选插件->搜索->Maven Integration->安装<br>
jenkins->系统管理->全局工具配置->maven安装->配置maven地址<br>
### 安装gitee插件
插件地址：https://gitee.com/oschina/Gitee-Jenkins-Plugin<br>
安装：jenkins->系统管理->管理插件->可选插件->搜索->Gitee->安装<br>
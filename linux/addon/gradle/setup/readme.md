# 运维笔记-Linux下gradle安装指南
## 前提条件
需要安装JDK<br>
## 安装gradle
下载地址<br>
```
https://gradle.org/releases/
```
解压缩<br>
```
unzip gradle.zip
```
## 配置gradle
配置环境变量<br>
```
vi /etc/profile
GRADLE_HOME=/root/gradle
PATH=$PATH:$GRADLE_HOME/bin
```
执行更改<br>
```
source /etc/profile
```
## 验证
```
gradle -v
```
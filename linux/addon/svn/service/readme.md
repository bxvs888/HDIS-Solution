# 运维笔记-Linux下SVN安装与配置
## 安装
```
yum install -y subversion
```
## 配置
### 初始化
```
svnadmin create /home/svn
```
### 配置
进入<br>
```
/home/svn
```
authz文件：配置用户以及用户文件访问权限<br>
```
#权限控制路径
[/project/ui]
#用户名=读写权限（r:read,w:write）
hzw=rw
```
passwd文件：配置用户与密码<br>
```
[users]
#用户名=密码
hzw=hzw
```
svnserve.conf文件：指定authz文件位置、passwd文件位置、数据文件位置<br>
```
[general]
anon-access=read
auth-access=write
#指定authz文件位置
password-db=passwd
#指定passwd文件位置
authz-db=authz
#指定数据文件位置
realm=/home/svn
```
### 启动
```
svnserve -d -r /home/svn
```
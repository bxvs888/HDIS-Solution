# 注意
可使用资源对象包括（不区分大小写）：
pod (po)
replicationcontroller (rc)
deployment (deploy)
service (svc)
daemonset (ds)
job
replicaset (rs)
# 部署Deployment
```
kubectl run ...[略]
```
# 部署Service
```
kubectl expose ...[略]
```
# 扩容与缩容
```
kubectl scale ...[略]
```
# 滚动更新
##更新
```
kubectl set image deploy/[DeploymentName] [PodLabel]=[ImageName]
kubectl set image deploy/nginx-deployment nginx=nginx:1.9.1
```
## 回滚
```
kubectl rollout undo deploy/[DeploymentName]
kubectl rollout undo deploy/nginx-deployment
```
# 部署配置文件
```
kubectl create configmap calendar-conf --from-file=/src/main/resources
```
# 查看资源列表
```
kubectl get [资源类型，如pod/deployment/service...] [其他参数]
kubectl get deploy
```
参数列表
--show-labelsc 查看lable
-o wide 查看详情
--namespace=xxx 查看某个命名空间的资源，若不指定，查看default空间的
# 查看资源细节
```
kubectl describe [资源类型，如pod/deployment/service...] [资源名称]
kubectl describe deploy user
```
# 使用配置文件创建资源（尽量使用此方式）
```
kubectl apply -f [文件地址]
kubectl apply -f Deployment.yaml
```
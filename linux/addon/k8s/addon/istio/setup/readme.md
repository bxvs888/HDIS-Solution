# K8S运维笔记-istio安装与配置(1.0.0版本)
## 安装(node节点执行)
### 下载
下载地址
```
https://github.com/istio/istio/releases/
```
解压缩
```
tar zxvf istio-1.0.0-linux.tar.gz
```
### 安装目录一览
bin istioctl客户端工具
install 针对不同环境的安装文档，这里使用的是kubernetes
samples 各种例子，很有用
tools 工具包，不知道用来干啥的
### 配置资源定义
```
cd istio-1.0.0
kubectl apply -f install/kubernetes/helm/istio/templates/crds.yaml
```
### 准备镜像
istio1.0.0需要以下包
```
gcr.io/istio-release/proxy_init:1.0.0
gcr.io/istio-release/proxyv2:1.0.0
gcr.io/istio-release/galley:1.0.0
gcr.io/istio-release/grafana:1.0.0
gcr.io/istio-release/mixer:1.0.0
gcr.io/istio-release/pilot:1.0.0
gcr.io/istio-release/citadel:1.0.0
gcr.io/istio-release/servicegraph:1.0.0
gcr.io/istio-release/sidecar_injector:1.0.0
quay.io/coreos/hyperkube:v1.7.6_coreos.0
docker.io/jaegertracing/all-in-one:1.5
docker.io/prom/prometheus:v2.3.1
docker.io/prom/statsd-exporter:v0.6.0
```
拉取镜像
```
docker pull istio/proxy_init:1.0.0
docker pull istio/proxyv2:1.0.0
docker pull istio/galley:1.0.0
docker pull istio/grafana:1.0.0
docker pull istio/mixer:1.0.0
docker pull istio/pilot:1.0.0
docker pull istio/citadel:1.0.0
docker pull istio/servicegraph:1.0.0
docker pull istio/sidecar_injector:1.0.0
docker pull zhenyuyang/hyperkube-v1.7.6_coreos.0
docker pull jaegertracing/all-in-one:1.5
docker pull prom/prometheus:v2.3.1
docker pull prom/statsd-exporter:v0.6.0
```
镜像更名
```
docker tag istio/proxy_init:1.0.0 gcr.io/istio-release/proxy_init:1.0.0
docker tag istio/proxyv2:1.0.0 gcr.io/istio-release/proxyv2:1.0.0
docker tag istio/galley:1.0.0 gcr.io/istio-release/galley:1.0.0
docker tag istio/grafana:1.0.0 gcr.io/istio-release/grafana:1.0.0
docker tag istio/mixer:1.0.0 gcr.io/istio-release/mixer:1.0.0
docker tag istio/pilot:1.0.0 gcr.io/istio-release/pilot:1.0.0
docker tag istio/citadel:1.0.0 gcr.io/istio-release/citadel:1.0.0
docker tag istio/servicegraph:1.0.0 gcr.io/istio-release/servicegraph:1.0.0
docker tag istio/sidecar_injector:1.0.0 gcr.io/istio-release/sidecar_injector:1.0.0
docker tag zhenyuyang/hyperkube-v1.7.6_coreos.0 quay.io/coreos/hyperkube:v1.7.6_coreos.0
docker tag jaegertracing/all-in-one:1.5 docker.io/jaegertracing/all-in-one:1.5
docker tag prom/prometheus:v2.3.1 docker.io/prom/prometheus:v2.3.1
docker tag prom/statsd-exporter:v0.6.0 docker.io/prom/statsd-exporter:v0.6.0
```
删除镜像
```
docker rmi istio/proxy_init:1.0.0
docker rmi istio/proxyv2:1.0.0
docker rmi istio/galley:1.0.0
docker rmi istio/grafana:1.0.0
docker rmi istio/mixer:1.0.0
docker rmi istio/pilot:1.0.0
docker rmi istio/citadel:1.0.0
docker rmi istio/servicegraph:1.0.0
docker rmi istio/sidecar_injector:1.0.0
docker rmi zhenyuyang/hyperkube-v1.7.6_coreos.0
docker rmi jaegertracing/all-in-one:1.5
docker rmi prom/prometheus:v2.3.1
docker rmi prom/statsd-exporter:v0.6.0
```
### 安装有权限的istio
```
kubectl apply -f install/kubernetes/istio-demo-auth.yaml
```
### 配置istio-sidecar-injector（流量代理工具）(与命名空间相关)
```
kubectl label namespace wowgo-develop istio-injection=enabled
kubectl label namespace wowgo-develop istio-injection=disabled
```
### 删除istio
kubectl delete -f install/kubernetes/istio-demo-auth.yaml
kubectl delete -f install/kubernetes/helm/istio/templates/crds.yaml -n istio-system
### 安装istioctl
注意：在devops服务器上安装istioctl，node节点可不用安装
```
vi /etc/profile
PATH=/root/istio/bin:$PATH
source /etc/profile
```
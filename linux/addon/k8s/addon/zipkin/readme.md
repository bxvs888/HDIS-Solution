# zipkin与linkerd集成
### 第一步：安装zipkin
运行zipkin.yml文件，即可完成zipkin安装
### 第二步：修改linkerd.yaml文件，并且重新启动linkerd服务
在linkerd.yaml文件里ConfigMap配置中telemetry属性中添加
```
    - kind: io.l5d.zipkin
      host: 10.102.2.82
      port: 9410
      sampleRate: 1.0
```
其中host为zipkin的clusterIP地址
修改完毕后重新运行linkerd.yaml中的ConfigMap与DaemonSet
### 第三步：执行linkerd路由命令验证
```
http_proxy=http://[linkerdIp]:4140 curl -s http://hello
http_proxy=http://10.105.185.141:4140 curl -s http://hello
```
### 第四步：查看zipkin的dashboard
使用物理地址与暴露的端口查看dashboard
```
http://10.248.37.254:9411
```
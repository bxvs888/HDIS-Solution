# heapster安装与配置
## 下载docker镜像
```
docker pull k8s.gcr.io/heapster-grafana-amd64:v4.4.3
docker pull k8s.gcr.io/heapster-amd64:v1.4.2
docker pull k8s.gcr.io/heapster-influxdb-amd64:v1.3.3
```
## 安装heapster
按顺序运行以下文件
heapster-rbac.yaml
influxdb.yaml
heapster.yaml
grafana.yaml
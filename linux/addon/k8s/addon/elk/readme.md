# ELK安装与配置
## 下载ELK镜像
请分别下载elasticsearch、logstash、kibana的镜像
## 安装ELK
### 写入configmap
kubectl create configmap kibana-config --from-file=kibana.yml
kubectl create configmap logstash-pipeline-config --from-file=logstash.conf
### 按顺序，使用K8S-dashboard运行K8S-yaml文件
**注意：镜像地址需修改**
elasticsearch-deployment.yaml
elasticsearch-service.yaml
logstash-deploment.yaml
kibana-deployment.yaml
kibana-service.yaml
## 访问kibana
使用kibana-service.yaml定义的地址访问kibana
# K8S运维笔记-dashboard安装与配置
[点击查看官方文档地址](https://kubernetes.io/zh/docs/tasks/access-application-cluster/web-ui-dashboard/)
## 在node服务器上安装
### 在node服务器上下载docker镜像
```
docker pull mirrorgooglecontainers/kubernetes-dashboard-amd64:v1.8.3
docker tag mirrorgooglecontainers/kubernetes-dashboard-amd64:v1.8.3 k8s.gcr.io/kubernetes-dashboard-amd64:v1.8.3
docker rmi mirrorgooglecontainers/kubernetes-dashboard-amd64:v1.8.3
```
### 在node服务器上安装dashboard
```
#创建集群超级管理员权限
kubectl apply -f cluster-role-admin.yml
#安装dashboard
kubectl apply -f kubernetes-dashboard.yaml
```
## 在devops上配置
### 前提条件
[《需要安装kubeadm》](https://gitee.com/w6513017/HDIS-Solution/tree/master/linux/addon/k8s/setup/kubeadm)<br>
### 在devops服务器上配置dashboard
```
#复制master服务器的$HOME/.kube/config文件到devops服务器的$HOME/.kube/config下
mkdir -p $HOME/.kube
kubectl get nodes
```
将kubectl-proxy.service拷贝至/etc/systemd/system<br>
kubectl-proxy.service<br>
运行命令执行配置<br>
```
systemctl daemon-reload
systemctl enable kubectl-proxy.service
systemctl restart kubectl-proxy.service
journalctl -xe
```
## 访问dashboard
地址
```
http://localhost:9090/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/
```
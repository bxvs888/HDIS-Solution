# k8s运维文档
## 注意
由于k8s更新迅速，可能会涉及到文档过期问题。
教程最好配合官方教程一并阅读。
教程地址：
```
https://kubernetes.io/docs/home/
```
## addon
k8s插件的安装、使用、运维文档。
包含：dashboard、dns、elk、heapster、ingress、traefik、zipkin
## order
k8s常用命令文档
## setup
k8s安装文档
## yaml
k8s各种组件模板
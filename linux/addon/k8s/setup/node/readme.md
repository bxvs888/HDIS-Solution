# K8S运维笔记-使用kubeadm安装K8S
## 安装Node
### 前提条件
[《需要安装docker》](https://gitee.com/w6513017/HDIS-Solution/tree/master/linux/addon/docker/setup)<br>
[《需要安装kubeadm》](https://gitee.com/w6513017/HDIS-Solution/tree/master/linux/addon/k8s/setup/kubeadm)<br>
至少需要2核2G的机器，Node节点要运行服务，按须提供。<br>
### 准备镜像
镜像下载地址：https://hub.docker.com/u/mirrorgooglecontainers/
1.11.2版本需要如下包，可使用阿里云容器镜像服务在海外下载镜像
```
k8s.gcr.io/kube-proxy-amd64:v1.11.2
k8s.gcr.io/pause:3.1
```
拉取镜像
```
docker pull mirrorgooglecontainers/kube-proxy-amd64:v1.11.2
docker pull mirrorgooglecontainers/pause-amd64:3.1
```
镜像更名
```
docker tag mirrorgooglecontainers/kube-proxy-amd64:v1.11.2 k8s.gcr.io/kube-proxy-amd64:v1.11.2
docker tag mirrorgooglecontainers/pause-amd64:3.1 k8s.gcr.io/pause:3.1
```
删除旧镜像
```
docker rmi mirrorgooglecontainers/kube-proxy-amd64:v1.11.2
docker rmi mirrorgooglecontainers/pause-amd64:3.1
```
### 在master节点上查看join命令需要的值
token
```
kubeadm token list
```
hash
```
openssl x509 -pubkey -in /etc/kubernetes/pki/ca.crt | openssl rsa -pubin -outform der 2>/dev/null | openssl dgst -sha256 -hex | sed 's/^.* //'
```
master-ip:master-port
```
cat /etc/kubernetes/kubelet.conf
```
### 安装
```
kubeadm join --token <token> <master-ip>:<master-port> --discovery-token-ca-cert-hash sha256:<hash>
kubeadm join --token 6w4krr.0jecbasna9n2wyrs 172.16.240.208:6443 --discovery-token-ca-cert-hash sha256:e7ca4b7e85a8b1802e32ea2438bfe9fbff35c6f31134d77ceaa0d241ec2fb315
```
### 配置kubectl
```
mkdir -p $HOME/.kube
```
复制master的config文件到node的.kube文件夹下
```
chown $(id -u):$(id -g) $HOME/.kube/config
```
### 失败处理
查询日志，查明原因
```
journalctl -f -u kubelet
```
删除node节点
```
kubectl drain <node name> --delete-local-data --force --ignore-daemonsets
kubectl delete node <node name>
```
node上重置节点
```
kubeadm reset
```
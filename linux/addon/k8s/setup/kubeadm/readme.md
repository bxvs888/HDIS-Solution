# K8S运维笔记-使用kubeadm安装K8S
## 安装kubeadm
更新阿里云安装源<br>
```
cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=http://mirrors.aliyun.com/kubernetes/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=0
repo_gpgcheck=0
gpgkey=http://mirrors.aliyun.com/kubernetes/yum/doc/yum-key.gpg
       http://mirrors.aliyun.com/kubernetes/yum/doc/rpm-package-key.gpg
EOF
```
配置<br>
```
setenforce 0
cat <<EOF >  /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF
```
安装<br>
```
yum install -y kubelet kubeadm kubectl --disableexcludes=kubernetes
systemctl enable kubelet && systemctl start kubelet
```
安装验证<br>
```
kubectl version
```
客户端访问验证<br>
复制master服务器的“$HOME/.kube/config”文件到devops服务器的“$HOME/.kube/config”下<br>
```
mkdir -p $HOME/.kube
kubectl get nodes
```
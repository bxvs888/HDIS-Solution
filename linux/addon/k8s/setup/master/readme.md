# K8S运维笔记-使用kubeadm安装K8S
## 安装Master
### 前提条件
[《需要安装docker》](https://gitee.com/w6513017/HDIS-Solution/tree/master/linux/addon/docker/setup)<br>
[《需要安装kubeadm》](https://gitee.com/w6513017/HDIS-Solution/tree/master/linux/addon/k8s/setup/kubeadm)<br>
至少需要2核2G的机器，使用推荐配置即可。<br>
### 准备镜像
查看镜像所需包
```
kubeadm config images list
```
1.11.2版本需要如下包
```
k8s.gcr.io/kube-apiserver-amd64:v1.11.2
k8s.gcr.io/kube-controller-manager-amd64:v1.11.2
k8s.gcr.io/kube-scheduler-amd64:v1.11.2
k8s.gcr.io/kube-proxy-amd64:v1.11.2
k8s.gcr.io/pause:3.1
k8s.gcr.io/etcd-amd64:3.2.18
k8s.gcr.io/coredns:1.1.3
quay.io/calico/typha:v3.1.7
quay.io/calico/node:v3.1.7
quay.io/calico/cni:v3.1.7
```
拉取镜像
```
docker pull mirrorgooglecontainers/kube-apiserver-amd64:v1.11.2
docker pull mirrorgooglecontainers/kube-controller-manager-amd64:v1.11.2
docker pull mirrorgooglecontainers/kube-scheduler-amd64:v1.11.2
docker pull mirrorgooglecontainers/kube-proxy-amd64:v1.11.2
docker pull mirrorgooglecontainers/pause-amd64:3.1
docker pull mirrorgooglecontainers/etcd-amd64:3.2.18
docker pull coredns/coredns:1.1.3
docker pull calico/typha:v3.1.7
docker pull calico/node:v3.1.7
docker pull calico/cni:v3.1.7
```
镜像更名
```
docker tag mirrorgooglecontainers/kube-apiserver-amd64:v1.11.2 k8s.gcr.io/kube-apiserver-amd64:v1.11.2
docker tag mirrorgooglecontainers/kube-controller-manager-amd64:v1.11.2 k8s.gcr.io/kube-controller-manager-amd64:v1.11.2
docker tag mirrorgooglecontainers/kube-scheduler-amd64:v1.11.2 k8s.gcr.io/kube-scheduler-amd64:v1.11.2
docker tag mirrorgooglecontainers/kube-proxy-amd64:v1.11.2 k8s.gcr.io/kube-proxy-amd64:v1.11.2
docker tag mirrorgooglecontainers/pause-amd64:3.1 k8s.gcr.io/pause:3.1
docker tag mirrorgooglecontainers/etcd-amd64:3.2.18 k8s.gcr.io/etcd-amd64:3.2.18
docker tag coredns/coredns:1.1.3 k8s.gcr.io/coredns:1.1.3
docker tag calico/typha:v3.1.7 quay.io/calico/typha:v3.1.7
docker tag calico/node:v3.1.7 quay.io/calico/node:v3.1.7
docker tag calico/cni:v3.1.7 quay.io/calico/cni:v3.1.7
```
删除旧镜像
```
docker rmi mirrorgooglecontainers/kube-apiserver-amd64:v1.11.2
docker rmi mirrorgooglecontainers/kube-controller-manager-amd64:v1.11.2
docker rmi mirrorgooglecontainers/kube-scheduler-amd64:v1.11.2
docker rmi mirrorgooglecontainers/kube-proxy-amd64:v1.11.2
docker rmi mirrorgooglecontainers/pause-amd64:3.1
docker rmi mirrorgooglecontainers/etcd-amd64:3.2.18
docker rmi coredns/coredns:1.1.3
docker rmi calico/typha:v3.1.7
docker rmi calico/node:v3.1.7
docker rmi calico/cni:v3.1.7
```
### 安装
--pod-network-cidr 指定安装Calico网络
--kubernetes-version=1.11.2 指定安装k8s具体版本，指定后将不用上网扫描
```
kubeadm init --pod-network-cidr=192.168.0.0/16 --kubernetes-version=1.11.2
```
### 配置kubectl
```
mkdir -p $HOME/.kube
cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
chown $(id -u):$(id -g) $HOME/.kube/config
```
### 配置网络
```
kubectl apply -f rbac-kdd.yaml
kubectl apply -f calico.yaml
```
### 配置端口范围
```
vi /etc/kubernetes/manifests/kube-apiserver.yaml
--service-node-port-range=1-65535
```
### 失败处理
查询日志，查明原因
```
journalctl -f -u kubelet
```
master上重置节点
```
kubeadm reset
```
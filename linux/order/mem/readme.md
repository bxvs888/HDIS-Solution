# 查看内存使用精简情况
```
free -m
```
# 查看内存使用详细情况
```
cat /proc/meminfo
```